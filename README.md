# refinery-deequ-python

Experiments with interfacing `refinery-source` with Python.

This repo contains a thin wrapper that integrates `pydeequ` with the [DPE
Data Quality Framework](https://wikitech.wikimedia.org/wiki/Data_Engineering/Data_Quality).

This repo is installable as a source dist with:
```
pip install -U git+https://gitlab.wikimedia.org/repos/data-engineering/refinery-deequ-python@main
```

The module requires `refinery-job`, `refinery-spark`, `refinery-core` deps at runtime. It depends on spark <= 3.10
and spark 3.1 (pip-installable with the `[provided]` or `[test]` extra require).
On stat nodes they are available on hdfs caches, and must be loaded in the
Spark session prior to importing this module
(and pydeequ).

Alternatively, follow the instruction below to get started.

## API

The core functionality is exposed via `refinery_python.dataset.HivePartition`,
`refinery_python.dq.DeequAnalyzersToDataQualityMetrics`
and `refinery_python.dq.DeequVerificationSuiteToDataQualityAlerts`
Their APIs resemble their Java counterparts.

For worked out examples see `examples/`

#### Example
```python
from refinery_python.dataset import HivePartition
from refinery_python.dq import DeequAnalyzersToDataQualityMetrics

# ...
# Run AnalysisRunner on a pydeequ repository
# ...

partition = HivePartition(
    spark,
    "wmf",
    "webrequest",
    None,
    {
        "source_key": "testDataset",
        "year": "2023",
        "month": "11",
        "day": "7",
        "hour": "0",
    },
)


metrics = DeequAnalyzersToDataQualityMetrics(spark, repository, partition, "123")
metrics.getAsDataFrame().show()
```

## Development

Install `refinery_spark` and its python runtime deps with
```
python -m venv venv
source venv/bin/activate
pip install ".[test]" # install test deps, incluing pyspark 3.10.
```

Install runtime Java deps with
```
make install_runtime_deps
```
This will download refinery into a `./jars` directory.

Run an example metrics pipeline with
`spark-submit --jars ./jars/refinery-spark.jar,./jars/refinery-core.jar,./jars/refinery-job-shaded.jar examples/metrics.py`.

## Test

Run tests with
```
make test
```
