#!/bin/env python
# Install refinery jars with:
# 	make install_runtime_deps
# Run with:
# 	spark-submt --jars ./jars/refinery-spark.jar,./jars/refinery-core.jar,./jars/refinery-job-shaded.jar examples/metrics.py
import os

# Initialize pydeequ. This code is taken from the library example
# at https://pypi.org/project/pydeequ/.
# We'll setup some analyzers, and pass their results to refinery's
# DeequAnalyzersToDataQualityMetrics scala SerDe.
os.environ["SPARK_VERSION"] = "3.1"
import pydeequ
from pydeequ.analyzers import *
from pydeequ.repository import *
from pyspark.sql import Row, SparkSession

from refinery_python.dataset import HivePartition
from refinery_python.dq import DeequAnalyzersToDataQualityMetrics

spark = (
    SparkSession.builder.appName("DQ metrics example").master("local[2]").getOrCreate()
)

df = spark.sparkContext.parallelize(
    [Row(a="foo", b=1, c=5), Row(a="bar", b=2, c=6), Row(a="baz", b=3, c=None)]
).toDF()

repository: InMemoryMetricsRepository = InMemoryMetricsRepository(spark)
resultKey: ResultKey = ResultKey(
    spark, ResultKey.current_milli_time(), {"tag": "sometag"}
)

AnalysisRunner(spark).onData(df).addAnalyzer(Size()).addAnalyzer(
    Completeness("b")
).useRepository(repository).saveOrAppendResult(resultKey).run()

partition = HivePartition(
    spark,
    "wmf",
    "webrequest",
    None,
    {
        "source_key": "testDataset",
        "year": "2023",
        "month": "11",
        "day": "7",
        "hour": "0",
    },
)


metrics = DeequAnalyzersToDataQualityMetrics(spark, repository, partition, "123")

# Note: getAsDataFrame() will return a JavaObject
metrics.getAsDataFrame().show()

# Shut down the Spark session to prevent any hanging processes.
spark.sparkContext._gateway.shutdown_callback_server()
spark.stop()