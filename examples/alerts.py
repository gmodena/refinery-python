#!/bin/env python
# Install refinery jars with:
# 	make install_runtime_deps
# Run with:
# 	spark-submt --jars ./jars/refinery-spark.jar,./jars/refinery-core.jar,./jars/refinery-job-shaded.jar examples/alerts.py
import os

# Initialize pydeequ. This code is taken from the library example
# at https://pypi.org/project/pydeequ/.
# We'll setup some analyzers, and pass their results to refinery's
# DeequAnalyzersToDataQualityMetrics scala SerDe.
os.environ["SPARK_VERSION"] = "3.1"
import sys

import pydeequ
from pydeequ.checks import *
from pydeequ.verification import *
from pyspark.sql import Row, SparkSession

from refinery_python.dataset import HivePartition
from refinery_python.dq import DeequVerificationSuiteToDataQualityAlerts

spark = (
    SparkSession.builder.appName("DQ alerts example").master("local[2]").getOrCreate()
)

df = spark.sparkContext.parallelize(
    [Row(a="foo", b=1, c=5), Row(a="bar", b=2, c=6), Row(a="baz", b=3, c=None)]
).toDF()

partition = HivePartition(
    spark,
    "wmf",
    "webrequest",
    None,
    {
        "source_key": "testDataset",
        "year": "2023",
        "month": "11",
        "day": "7",
        "hour": "0",
    },
)

check = Check(spark, CheckLevel.Warning, "Review Check")

verification_suite = (
    VerificationSuite(spark)
    .onData(df)
    .addCheck(
        check.hasSize(lambda x: x >= 3)
        .hasMin("b", lambda x: x == 0)
        .isComplete("c")
        .isUnique("a")
        .isContainedIn("a", ["foo", "bar", "baz"])
        .isNonNegative("b")
    )
    .run()
)

# The API is slightly different than the Scala one. Here pass a `verification_suite` object
# directly, whereas in scala DeequVerificationSuiteToDataQualityAlerts expects a Map[Check, CheckResult].
# That object is extracted from verification_suite in the Python
# DeequVerificationSuiteToDataQualityAlerts constructor, and passed down to the JVM instance of the
# wrapped class.
alerts = DeequVerificationSuiteToDataQualityAlerts(
    spark, verification_suite, partition, "123"
)

# Note: getAsDataFrame() will return a JavaObject
alerts.getAsDataFrame().show()

# Shut down the Spark session to prevent any hanging processes.
spark.sparkContext._gateway.shutdown_callback_server()
spark.stop()