from typing import Any, Dict

from py4j.java_gateway import JavaObject
from pyspark.sql import SparkSession


class PythonUtils:
    """
    Utility class for Python to Java interoperability.

    Parameters:
        spark (SparkSession): The Spark session.

    Attributes:
        jvm: The Java Virtual Machine instance.

    Example:
        utils = PythonUtils(spark)
    """

    def __init__(self, spark: SparkSession):
        """
        Initializes PythonUtils.

        This is a tiny wrapper around Spark's py4j gateway,
        and provides conversion method for immutable types.

        Parameters:
            spark (SparkSession): The Spark session.
        """
        self._jvm = spark.sparkContext._gateway.jvm

    @property
    def jvm(self):
        return self._jvm

    def to_scala_list_map(self, pmap: Dict[Any, Any]) -> JavaObject:
        """
        Converts a Python dictionary to a Scala ListMap.

        Parameters:
            pmap (Dict[Any, Any]): The Python dictionary to be converted.

        Returns:
            JavaObject: A Scala ListMap object containing the key-value pairs.

        Example:
            pmap = {"key1": "value1", "key2": "value2"}
            scala_map = utils.to_scala_list_map(pmap)
        """
        JSome = self.jvm.scala.Some
        JTuple2 = self.jvm.scala.Tuple2
        list_map = self.jvm.scala.collection.immutable.ListMap()
        for key, value in pmap.items():
            tpl = JTuple2(key, JSome(value))
            list_map = getattr(list_map, "$plus")(tpl)
        return list_map
