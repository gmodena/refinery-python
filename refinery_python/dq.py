from py4j.java_gateway import JavaObject
from pyspark.sql import SparkSession
from pydeequ.repository import MetricsRepository

from .java import PythonUtils


class DeequAnalyzersToDataQualityMetrics:
    """
    Converts Deequ analyzers result to data quality metrics.

    Parameters:
        spark (SparkSession): The Spark session that holds a py4j gateway.
        repository (pydeequ.repository.MetricsRepository): A repository containing deequ metrics.
        partition (JavaObject): The HivePartition object.
        run_id (str): The ID of the analyzers run, typically an airflow dag_id.

    Returns:
        DeequAnalyzersToDataQualityMetrics: An instance of the DeequAnalyzersToDataQualityMetrics class.

    Note:
        This class is a Python wrapper for the corresponding Java class org.wikimedia.analytics.refinery.spark.deequ.DeequAnalyzersToDataQualityMetrics.

    Example:
        analyzer_metrics = DeequAnalyzersToDataQualityMetrics(spark, analyzers_result, partition, "run_123")
    """

    def __new__(
        cls,
        spark: SparkSession,
        repository: MetricsRepository,
        partition: JavaObject,
        run_id: str,
    ) -> "DeequAnalyzersToDataQualityMetrics":
        java = PythonUtils(spark)
        j_DeequAnalyzersToDataQualityMetrics = (
            java.jvm.org.wikimedia.analytics.refinery.spark.deequ.DeequAnalyzersToDataQualityMetrics
        )
        # Java DeequAnalyzersToDataQualityMetrics takes an instance of AnalyzersResult as input.
        # This object is not available from the Python api, so we drop into jvm with
        # a call to repository.repository instead. This should also ensure that compute metrics are not passed over the
        # python <-> jvm wire (we access jvm objects directly).
        analyzers_result = repository.repository.load().get()
        return j_DeequAnalyzersToDataQualityMetrics(
            spark._jsparkSession,
            analyzers_result,
            partition,
            run_id,
        )


class DeequVerificationSuiteToDataQualityAlerts:
    """
    Converts Deequ analyzers result to data quality metrics.

    Parameters:
        spark (SparkSession): The Spark session that holds a py4j gateway.
        verification_suite (JavaObject): The result of Deequ constraint checker.
        partition (JavaObject): The HivePartition object.
        run_id (str): The ID of the analyzers run, typically an airflow dag_id.

    Returns:
        DeequVerificationSuiteToDataQualityAlerts: An instance of the DeequVerificationSuiteToDataQualityAlerts class.

    Note:
        This class is a Python wrapper for the corresponding Java class org.wikimedia.analytics.refinery.spark.deequ.DeequVerificationSuiteToDataQualityAlerts.

    Example:
        alerts = DeequVerificationSuiteToDataQualityAlerts(spark, verification_suite, partition, "run_123")
    """

    def __new__(
        cls,
        spark: SparkSession,
        verification_suite: JavaObject,
        partition: JavaObject,
        run_id: str,
    ) -> "DeequAnalyzersToDataQualityMetrics":
        java = PythonUtils(spark)
        j_DeequVerificationSuiteToDataQualityAlerts = (
            java.jvm.org.wikimedia.analytics.refinery.spark.deequ.DeequVerificationSuiteToDataQualityAlerts
        )

        # verificationRun returns the Java object wrapped by pydeequ.
        # verificationRun.checkResults() returns the Java Map[Check, CheckResult]
        # object required by the
        # org.wikimedia.analytics.refinery.spark.deequ.DeequVerificationSuiteToDataQualityAlerts
        # constructor.
        # TODO: maybe we should align the Java constructor to pydeequ APIs,
        #       and have it extract checkResults() from a verificationRun instance.
        check_results = verification_suite.verificationRun.checkResults()
        return j_DeequVerificationSuiteToDataQualityAlerts(
            spark._jsparkSession, check_results, partition, run_id
        )
