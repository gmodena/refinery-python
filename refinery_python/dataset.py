from typing import Dict, Optional

from py4j.java_gateway import JavaClass
from pyspark.sql import SparkSession

from .java import PythonUtils


class HivePartition:
    """
    Represents a partition in a Hive table.

    Parameters:
        spark (SparkSession): The Spark session.
        database (str): The name of the Hive database.
        table (str): The name of the Hive table.
        location (Optional[str], optional): The location of the partition. Defaults to None.
        partitions (Optional[Dict[str, str]], optional): The partition key-value pairs. Defaults to None.

    Returns:
        HivePartition: An instance of the HivePartition class.

    Note:
        This class is a Python wrapper for the corresponding Java class org.wikimedia.analytics.refinery.core.HivePartition.

    Example:
        partition = HivePartition(spark, "my_database", "my_table", location="/path/to/partition", partitions={"year": "2023", "month": "01"})
    """

    def __new__(
        cls,
        spark: SparkSession,
        database: str,
        table: str,
        location: Optional[str] = None,
        partitions: Optional[Dict[str, str]] = None,
    ) -> "HivePartition":
        java = PythonUtils(spark)
        j_HivePartition: JavaClass = (
            java.jvm.org.wikimedia.analytics.refinery.core.HivePartition
        )
        return j_HivePartition(
            database, table, location, java.to_scala_list_map(partitions)
        )
