from py4j.java_gateway import JavaObject
from pydeequ.checks import Check, CheckLevel
from pydeequ.verification import VerificationSuite

from refinery_python.dq import DeequVerificationSuiteToDataQualityAlerts


def test_alerts_dataframe(spark, partition, data):
    check = Check(spark, CheckLevel.Warning, "Review Check")

    verification_suite = (
        VerificationSuite(spark)
        .onData(data)
        .addCheck(
            check.hasSize(lambda x: x >= 3)
            .hasMin("b", lambda x: x == 0)
            .isComplete("c")
            .isUnique("a")
            .isContainedIn("a", ["foo", "bar", "baz"])
            .isNonNegative("b")
        )
        .run()
    )

    alerts = DeequVerificationSuiteToDataQualityAlerts(
        spark, verification_suite, partition, "123"
    )

    expected_columns = set([
        "source_table",
        "partition_id",
        "partition_ts",
        "status",
        "severity_level",
        "value",
        "constraint",
        "error_message",
        "pipeline_run_id",
    ])
    expected_size = 6

    alerts_df = alerts.getAsDataFrame()

    assert isinstance(alerts_df, JavaObject)
    assert set(alerts_df.columns()) == expected_columns
    assert alerts_df.count() == expected_size
