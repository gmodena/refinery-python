from py4j.java_gateway import JavaObject
from pydeequ.analyzers import AnalysisRunner, Completeness, Size
from pydeequ.repository import InMemoryMetricsRepository, ResultKey

from refinery_python.dq import DeequAnalyzersToDataQualityMetrics


def test_metrics_dataframe(spark, partition, data):
    repository: InMemoryMetricsRepository = InMemoryMetricsRepository(spark)
    resultKey: ResultKey = ResultKey(
        spark, ResultKey.current_milli_time(), {"tag": "sometag"}
    )

    AnalysisRunner(spark).onData(data).addAnalyzer(Size()).addAnalyzer(
        Completeness("b")
    ).useRepository(repository).saveOrAppendResult(resultKey).run()

    metrics = DeequAnalyzersToDataQualityMetrics(
        spark, repository, partition, "123"
    )
    metrics_df = metrics.getAsDataFrame()

    expected_size = 2
    expected_columns = set(
        [
            "dataset_date",
            "tags",
            "entity",
            "instance",
            "value",
            "name",
            "source_table",
            "pipeline_run_id",
            "partition_id",
            "partition_ts",
        ]
    )

    assert isinstance(metrics_df, JavaObject)
    assert set(metrics_df.columns()) == expected_columns
    assert metrics_df.count() == expected_size
