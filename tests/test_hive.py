def test_hive_partition(partition):
    assert (
        partition.hiveQL() == 'source_key="testDataset",year=2023,month=11,day=7,hour=0'
    )
