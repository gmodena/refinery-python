import os

import pytest
from pyspark.sql import Row, SparkSession

from refinery_python.dataset import HivePartition

# The spark version supported by refinery
os.environ["SPARK_VERSION"] = "3.1"

@pytest.fixture(scope="session")
def spark(spark_session: SparkSession):
    """
    A pytest yield finalizer to make sure
    pydeequ does not leave any spark / py4j process runnng.

    Wraps the `spark_session` fixture provided by the pytest-spark
    package.

    :param spark_session:
    :return:
    """
    yield spark_session

    spark_session.sparkContext._gateway.shutdown_callback_server()
    spark_session.stop()

@pytest.fixture(scope="session")
def partition(spark: SparkSession):
    return HivePartition(
        spark,
        "wmf",
        "webrequest",
        None,
        {
            "source_key": "testDataset",
            "year": "2023",
            "month": "11",
            "day": "7",
            "hour": "0",
        },
    )


@pytest.fixture(scope="session")
def data(spark: SparkSession):
    return spark.sparkContext.parallelize(
        [Row(a="foo", b=1, c=5), Row(a="bar", b=2, c=6), Row(a="baz", b=3, c=None)]
    ).toDF()
