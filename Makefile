# Do not modify PROJECT_VERSION manually. Use bump2version instead.
PROJECT_VERSION := 0.1.0.dev

# URL to Wikimedia's maven repo containing refinery deps
ARCHIVA_REPO := https://archiva.wikimedia.org/repository/releases/org/wikimedia/analytics/refinery/

# Version of refinery-source we are wrapping
REFINERY_VERSION := 0.2.54

# Location of refinery Java dependencies.
JARS_DIR := $(shell pwd)/jars

# Sphinx documentation path
DOCS_DIR := docs

# Download runtime Java dependencies. These are needed to run tests.
install_runtime_deps:
	mkdir -p ${JARS_DIR}
	@if [ ! -f "${JARS_DIR}/refinery-job-shaded.jar" ]; then \
	  	curl -o ${JARS_DIR}/refinery-job-shaded.jar ${ARCHIVA_REPO}/job/refinery-job/${REFINERY_VERSION}/refinery-job-${REFINERY_VERSION}-shaded.jar; \
	fi
	@if [ ! -f "${JARS_DIR}/refinery-spark.jar" ]; then \
		curl -o ${JARS_DIR}/refinery-spark.jar ${ARCHIVA_REPO}/spark/refinery-spark/${REFINERY_VERSION}/refinery-spark-${REFINERY_VERSION}.jar; \
	fi
	@if [ ! -f "${JARS_DIR}/refinery-core.jar" ]; then \
		curl -o ${JARS_DIR}/refinery-core.jar ${ARCHIVA_REPO}/core/refinery-core/${REFINERY_VERSION}/refinery-core-${REFINERY_VERSION}.jar; \
	fi

# Run the pytest suite.
test:	install_runtime_deps
	pytest

clean:
	rm -r ${JARS_DIR}

.PHONY: test install_runtime_deps clean
